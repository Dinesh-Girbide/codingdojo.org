// Implementation to check if the given year is leap year or not
public class LeapYearProgramImpl{

    public Boolean isLeapYear(int leapyear){
        Boolean isLeapYear = false;

        if(((leapyear % 4 == 0) && (leapyear % 100!=0)) ||
         (leapyear % 400 == 0 )){
            isLeapYear = true;
        }

        return isLeapYear;
    }
}
